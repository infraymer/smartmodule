const conf = require('./config').config;
var autobahn = require('autobahn');
const db = require('./querydb');
var queue = require('./queue-function');
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';

// npm install mysql2
// npm install log4js
// npm install autobahn
// npm install svmq

//очереди сообщений
var queueWrite = require('svmq').open(555);
var queueRead = require('svmq').open(777);

var sendMsgToQueue = function (data) {
    queue.pushQueue(queueWrite, data);
};

// DB connect
db.connect().then(() => {
    logger.info("Database - CONNECTED");
}).catch((err) => {
    logger.error("Database - ERROR: " + err);
    throw err;
});

// wamp connect
var connection = new autobahn.Connection({
    url: `ws://${conf.server.address}:${conf.server.port}/ws`,
    realm: 'smart'
});

let getDevices = async function (args) {
    logger.info('API - getDevices - args:' + args);
    return await db.getDevices();
};

let setParamDevice = async function (args) {
    logger.info('API - setParamDevice - args:' + args);
    let func = await db.addFunction({idDevice: args[1], idFunction: args[2], value: args[3]});
    sendMsgToQueue(func);
};

connection.onopen = function (session) {
    logger.info('WAMP - CONNECTED');

    logger.info('WAMP - REGISTER - smart.subs.get_devices');
    session.register('smart.subs.get_devices', getDevices, {match: 'exact'});
    logger.info('WAMP - REGISTER - smart.subs.set_param_device');
    session.register('smart.subs.set_param_device', setParamDevice, {match: 'exact'});

    queue.readQueue(queueRead, (result) => {

        db.getDevice(result.idAddress, result.idDopAddress).then((device) => {
            logger.info('WAMP - PUBLISH - smart.subs.device');
            session.publish('smart.subs.device', [device]);
            let func = {
                idDevice: device.id,
                idFunction: result.idFunction,
                value: result.value
            };
            logger.info('WAMP - PUBLISH - smart.subs.function');
            session.publish('smart.subs.function', [JSON.stringify(func)]);
        }).catch((err) => {
            logger.error(err);
        });
    });
};

connection.open();
