const config = require('./config').config;
const mysql = require('mysql2/promise');
let connection;

let connect = async function () {
    connection = await mysql.createConnection(config.database);
};

let disconnect = async function () {
    connection.close();
};

let addFunction = async function (data) {
    let [result, fields1] = await connection.execute(
        `
        SELECT
			  func.number,
			  dop_addr.dop_addr as dopAddress,
			  device.address
			FROM func,
				 dop_addr
				   INNER JOIN device
					 ON dop_addr.id_dev = device.iddevice
			WHERE func.idfunc = ?
			AND dop_addr.id_dop_addr = ?
        `, [data.idFunction, data.idDevice]
    );
    data.numberFunction = result[0].number;
    data.idAddress = result[0].address;
    data.idDopAddress = result[0].dopAddress;
    return data;
};

let getRooms = async function () {
    let rooms = [];
    const [roomRows, roomFields] = await connection.execute('SELECT * FROM room');
    for (i = 0; i < roomRows.length; i++) {
        let room = {
            id: roomRows[i].idroom,
            name: roomRows[i].name,
            categories: []
        };
        rooms.push(room);
        const [categoryRows, categoryFields] = await connection.execute(
            'SELECT DISTINCT gi.idgroup, gi.name ' +
            'FROM group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d ' +
            'WHERE td.idgroup = gi.idgroup and ' +
            'da.idtype_device = td.idtype_device and ' +
            'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
            'fa.func_idfunc = f.idfunc and ' +
            'dr.id_dop_addr = da.id_dop_addr and ' +
            'dr.id_room = r.idroom and ' +
            'da.id_dev = d.iddevice and ' +
            'r.idroom = ? ' +
            'ORDER BY gi.idgroup', [room.id]
        );
        for (j = 0; j < categoryRows.length; j++) {
            let category = {
                id: categoryRows[j].idgroup,
                name: categoryRows[j].name,
                devices: []
            };
            room.categories.push(category);
            const [deviceRows, deviceFields] = await connection.execute(
                'select distinct td.name_type_device, da.id_dop_addr ' +
                'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d ' +
                'where td.idgroup = gi.idgroup and ' +
                'da.idtype_device = td.idtype_device and ' +
                'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
                'fa.func_idfunc = f.idfunc and ' +
                'dr.id_dop_addr = da.id_dop_addr and ' +
                'dr.id_room = r.idroom and ' +
                'da.id_dev = d.iddevice and ' +
                'r.idroom = ? and ' +
                'gi.idgroup = ? ' +
                'ORDER BY td.name_type_device', [room.id, category.id]
            );
            for (k = 0; k < deviceRows.length; k++) {
                let device = {
                    id: deviceRows[k].id_dop_addr,
                    name: deviceRows[k].name_type_device,
                    functions: []
                };
                category.devices.push(device);
                const [functionRows, functionFields] = await connection.execute(
                    'select f.name, f.idfunc, f2.`data`, f.write_enable, f.val_min, f.val_max ' +
                    'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d, function f2 ' +
                    'where td.idgroup = gi.idgroup and ' +
                    'da.idtype_device = td.idtype_device and ' +
                    'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
                    'fa.func_idfunc = f.idfunc and ' +
                    'dr.id_dop_addr = da.id_dop_addr and ' +
                    'dr.id_room = r.idroom and ' +
                    'da.id_dev = d.iddevice and ' +
                    'f2.idfunc = f.idfunc and f2.id_dop_addr = da.id_dop_addr and ' +
                    'r.idroom = ? and ' +
                    'gi.idgroup = ? and ' +
                    'td.name_type_device = ? and ' +
                    'da.id_dop_addr = ? ' +
                    'order by f.idfunc', [room.id, category.id, device.name, device.id]
                );
                for (l = 0; l < functionRows.length; l++) {
                    let func = {
                        id: functionRows[l].idfunc,
                        name: functionRows[l].name,
                        value: functionRows[l].data,
                        writeIsEnable: functionRows[l].write_enable.lastIndexOf(1) !== -1,
                        minValue: functionRows[l].val_min,
                        maxValue: functionRows[l].val_max
                    };
                    device.functions.push(func)
                }
            }
        }
    }
    return rooms;
};

let getDevicesOld = async function () {
    let devices = [];
    const [deviceRows, deviceFields] = await connection.execute(
        'select distinct td.name_type_device as name, da.id_dop_addr as id, r.idroom as idRoom, r.name as nameRoom, gi.idgroup as idCategory, gi.name as nameCategory ' +
        'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d ' +
        'where td.idgroup = gi.idgroup and ' +
        'da.idtype_device = td.idtype_device and ' +
        'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
        'fa.func_idfunc = f.idfunc and ' +
        'dr.id_dop_addr = da.id_dop_addr and ' +
        'dr.id_room = r.idroom and ' +
        'da.id_dev = d.iddevice', []
    );
    for (k = 0; k < deviceRows.length; k++) {
        let device = {
            id: deviceRows[k].id,
            name: deviceRows[k].name,
            room: {
                id: deviceRows[k].idRoom,
                name: deviceRows[k].nameRoom
            },
            category: {
                id: deviceRows[k].idCategory,
                name: deviceRows[k].nameCategory
            },
            functions: []
        };
        devices.push(device);
        const [functionRows, functionFields] = await connection.execute(
            'select f.name, f.idfunc, f2.`data`, f.write_enable, f.val_min, f.val_max, f.measure as meas ' +
            'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d, function f2 ' +
            'where td.idgroup = gi.idgroup and ' +
            'da.idtype_device = td.idtype_device and ' +
            'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
            'fa.func_idfunc = f.idfunc and ' +
            'dr.id_dop_addr = da.id_dop_addr and ' +
            'dr.id_room = r.idroom and ' +
            'da.id_dev = d.iddevice and ' +
            'f2.idfunc = f.idfunc and f2.id_dop_addr = da.id_dop_addr and ' +
            'r.idroom = ? and ' +
            'gi.idgroup = ? and ' +
            'td.name_type_device = ? and ' +
            'da.id_dop_addr = ? ' +
            'order by f.idfunc', [device.room.id, device.category.id, device.name, device.id]
        );
        for (l = 0; l < functionRows.length; l++) {
            let func = {
                id: functionRows[l].idfunc,
                name: functionRows[l].name,
                measure: functionRows[l].meas,
                value: parseInt(functionRows[l].data),
                isWriteAccess: functionRows[l].write_enable.lastIndexOf(1) !== -1,
                minValue: functionRows[l].val_min,
                maxValue: functionRows[l].val_max
            };
            device.functions.push(func)
        }
    }
    return devices;
};

let getDeviceOld = async function (addressDevice, dopAddressDevice) {
    const [deviceRows, deviceFields] = await connection.execute(
        'select distinct td.name_type_device as name, da.id_dop_addr as id, r.idroom as idRoom, r.name as nameRoom, gi.idgroup as idCategory, gi.name as nameCategory ' +
        'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d ' +
        'where td.idgroup = gi.idgroup and ' +
        'da.idtype_device = td.idtype_device and ' +
        'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
        'fa.func_idfunc = f.idfunc and ' +
        'dr.id_dop_addr = da.id_dop_addr and ' +
        'dr.id_room = r.idroom and ' +
        'da.id_dev = d.iddevice and ' +
        'd.address = ? and ' +
        'da.dop_addr = ?', [addressDevice, dopAddressDevice]
    );
    let device = {
        id: deviceRows[0].id,
        name: deviceRows[0].name,
        room: {
            id: deviceRows[0].idRoom,
            name: deviceRows[0].nameRoom
        },
        category: {
            id: deviceRows[0].idCategory,
            name: deviceRows[0].nameCategory
        },
        functions: []
    };
    const [functionRows, functionFields] = await connection.execute(
        'select f.name, f.idfunc, f2.`data`, f.write_enable, f.val_min, f.val_max, f.measure as meas ' +
        'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d, function f2 ' +
        'where td.idgroup = gi.idgroup and ' +
        'da.idtype_device = td.idtype_device and ' +
        'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
        'fa.func_idfunc = f.idfunc and ' +
        'dr.id_dop_addr = da.id_dop_addr and ' +
        'dr.id_room = r.idroom and ' +
        'da.id_dev = d.iddevice and ' +
        'f2.idfunc = f.idfunc and f2.id_dop_addr = da.id_dop_addr and ' +
        'r.idroom = ? and ' +
        'gi.idgroup = ? and ' +
        'td.name_type_device = ? and ' +
        'da.id_dop_addr = ? ' +
        'order by f.idfunc', [device.room.id, device.category.id, device.name, device.id]
    );
    for (l = 0; l < functionRows.length; l++) {
        let func = {
            id: functionRows[l].idfunc,
            name: functionRows[l].name,
            measure: functionRows[l].meas,
            value: parseInt(functionRows[l].data),
            isWriteAccess: functionRows[l].write_enable.lastIndexOf(1) !== -1,
            minValue: functionRows[l].val_min,
            maxValue: functionRows[l].val_max
        };
        device.functions.push(func)
    }
    return device;
};

let getDevices = async function () {
    let devices = [];
    const [deviceRows, deviceFields] = await connection.execute(
        `
        select 
        td.name_type_device as name, 
        da.id_dop_addr as id, 
        r.idroom as idRoom, 
        r.name as nameRoom, 
        gi.idgroup as idCategory, 
        gi.name as nameCategory
        from dop_addr da, type_device td, group_interf gi, device_room dr, room r
        where da.id_dop_addr = dr.id_dop_addr and
        dr.id_room = r.idroom and
        da.idtype_device = td.idtype_device and 
        td.idgroup = gi.idgroup
        `
        , []
    );
    for (k = 0; k < deviceRows.length; k++) {
        let device = {
            id: deviceRows[k].id,
            name: deviceRows[k].name,
            room: {
                id: deviceRows[k].idRoom,
                name: deviceRows[k].nameRoom
            },
            category: {
                id: deviceRows[k].idCategory,
                name: deviceRows[k].nameCategory
            },
            functions: []
        };
        devices.push(device);
        const [functionRows, functionFields] = await connection.execute(
            `
            select func.name AS name, 
             func.idfunc AS idfunc, 
             func.val_min AS val_min, 
             func.val_max AS val_max,
             function.data, 
             func.write_enable AS write_enable, 
             func.measure AS meas, 
             func.show_general as SHOW_GENERAL
             from func_has_dop_addr
             inner join dop_addr on func_has_dop_addr.dop_addr_id_dop_addr = dop_addr.id_dop_addr
             left join func on func_has_dop_addr.func_idfunc = func.idfunc
             left join function on function.id_dop_addr = dop_addr.id_dop_addr AND function.idfunc = func.idfunc
             left join device_room on device_room.id_dop_addr = dop_addr.id_dop_addr
             left join room on device_room.id_room = room.idroom
             left join type_device on dop_addr.idtype_device = type_device.idtype_device
             left join voc_general_view on func.general_view = voc_general_view.id_voc_gv
             where dop_addr.id_dop_addr = ?
             order by func.val_max
            `
            , [device.id]
        );
        for (l = 0; l < functionRows.length; l++) {
            let data = functionRows[l].data;
            let min = functionRows[l].val_min;
            if (data == null) data = min;
            let func = {
                id: functionRows[l].idfunc,
                name: functionRows[l].name,
                measure: functionRows[l].meas,
                value: parseInt(data),
                isWriteAccess: functionRows[l].write_enable.lastIndexOf(1) !== -1,
                minValue: functionRows[l].val_min,
                maxValue: functionRows[l].val_max
            };
            device.functions.push(func)
        }
    }
    return devices;
};

let getDevice = async function (addressDevice, dopAddressDevice) {
    const [deviceRows, deviceFields] = await connection.execute(
        'select distinct td.name_type_device as name, da.id_dop_addr as id, r.idroom as idRoom, r.name as nameRoom, gi.idgroup as idCategory, gi.name as nameCategory ' +
        'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d ' +
        'where td.idgroup = gi.idgroup and ' +
        'da.idtype_device = td.idtype_device and ' +
        'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
        'fa.func_idfunc = f.idfunc and ' +
        'dr.id_dop_addr = da.id_dop_addr and ' +
        'dr.id_room = r.idroom and ' +
        'da.id_dev = d.iddevice and ' +
        'd.address = ? and ' +
        'da.dop_addr = ?', [addressDevice, dopAddressDevice]
    );
    let device = {
        id: deviceRows[0].id,
        name: deviceRows[0].name,
        room: {
            id: deviceRows[0].idRoom,
            name: deviceRows[0].nameRoom
        },
        category: {
            id: deviceRows[0].idCategory,
            name: deviceRows[0].nameCategory
        },
        functions: []
    };
    const [functionRows, functionFields] = await connection.execute(
        'select f.name, f.idfunc, f2.`data`, f.write_enable, f.val_min, f.val_max, f.measure as meas ' +
        'from group_interf gi, type_device td, func f, func_has_dop_addr fa, dop_addr da, room r, device_room dr, device d, function f2 ' +
        'where td.idgroup = gi.idgroup and ' +
        'da.idtype_device = td.idtype_device and ' +
        'fa.dop_addr_id_dop_addr = da.id_dop_addr and ' +
        'fa.func_idfunc = f.idfunc and ' +
        'dr.id_dop_addr = da.id_dop_addr and ' +
        'dr.id_room = r.idroom and ' +
        'da.id_dev = d.iddevice and ' +
        'f2.idfunc = f.idfunc and f2.id_dop_addr = da.id_dop_addr and ' +
        'r.idroom = ? and ' +
        'gi.idgroup = ? and ' +
        'td.name_type_device = ? and ' +
        'da.id_dop_addr = ? ' +
        'order by f.idfunc', [device.room.id, device.category.id, device.name, device.id]
    );
    for (l = 0; l < functionRows.length; l++) {
        let func = {
            id: functionRows[l].idfunc,
            name: functionRows[l].name,
            measure: functionRows[l].meas,
            value: parseInt(functionRows[l].data),
            isWriteAccess: functionRows[l].write_enable.lastIndexOf(1) !== -1,
            minValue: functionRows[l].val_min,
            maxValue: functionRows[l].val_max
        };
        device.functions.push(func)
    }
    return device;
};

let kek = async function (id) {
    await connect();
    const [functionRows, functionFields] = await connection.execute(
        `
            select func.name AS name, 
             func.idfunc AS idfunc, 
             func.val_min AS val_min, 
             func.val_max AS val_max,
             function.data, 
             func.write_enable AS write_enable, 
             func.measure AS meas, 
             func.show_general as SHOW_GENERAL
             from func_has_dop_addr
             inner join dop_addr on func_has_dop_addr.dop_addr_id_dop_addr = dop_addr.id_dop_addr
             left join func on func_has_dop_addr.func_idfunc = func.idfunc
             left join function on function.id_dop_addr = dop_addr.id_dop_addr AND function.idfunc = func.idfunc
             left join device_room on device_room.id_dop_addr = dop_addr.id_dop_addr
             left join room on device_room.id_room = room.idroom
             left join type_device on dop_addr.idtype_device = type_device.idtype_device
             left join voc_general_view on func.general_view = voc_general_view.id_voc_gv
             where dop_addr.id_dop_addr = ?
             order by func.val_max
            `
        , [id]
    );
    console.log();
};

module.exports.connect = connect;
module.exports.disconnect = disconnect;
module.exports.getRooms = getRooms;
module.exports.getDevices = getDevices;
module.exports.getDevice = getDevice;
module.exports.addFunction = addFunction;
