var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';
module.exports.pushQueue = function (queue, data) {
    try {
        logger.debug("QUEUE - WRITE - : " + JSON.stringify(data));
        var address = data.idAddress;
        var func = data.numberFunction;
        var dopAddress = data.idDopAddress;
        var value = data.value;

        let com_dop_adr_1 = [2, 5, 6];
        let com_dop_adr_2 = [12];
        if (com_dop_adr_1.indexOf(dopAddress) !== -1) {
            var data_1 = 0;
            var data_0 = 0;
            if (value > 0) {
                data_0 = 1 << dopAddress;
            } else {
                data_1 = 1 << dopAddress
                // data_0 = 1 << (dopAddress + 4);
            }
            /*if (func === 5) {
                func = 6;
            }*/
            let d = '_' + address + '_1_' + func + '_' + data_1 + '_' + data_0;
            d = 's:11:\"' + d + '\"\;';
            logger.debug("QUEUE - WRITE - : " + d);
            queue.push(new Buffer(d), (err) => {
                if (err != null) logger.error("QUEUE - ERROR - : " + err);
            });
        } else if (com_dop_adr_2.indexOf(func) !== -1) {
            value = value * 2;
            let d = '_' + address + '_1_' + func + '_' + dopAddress + '_' + value;
            d = 's:11:\"' + d + '\"\;';
            logger.debug("QUEUE - WRITE - : " + d);
            queue.push(new Buffer(d), (err) => {
                if (err != null) logger.error("QUEUE - ERROR - : " + err);
            });
        }
    } catch (e) {
        logger.error(e);
    }
};

module.exports.readQueue = function (queue, result) {
    queue.on('data', (data) => {
        try {
            logger.debug("QUEUE - READ - : " + data);
            // var array = data.toString().split('"')[1].split('_');
            var array = data.toString().split('_');
            if (array.length = 6) {
                var msg = {
                    idAddress: array[1],
                    idFunction: array[3],
                    idDopAddress: array[2],
                    value: parseInt(array[4])
                };
                logger.debug("QUEUE - READ - return result - " + JSON.stringify(msg));
                result(msg);
            }
        } catch (e) {
            logger.error(e);
        }
    });
};
