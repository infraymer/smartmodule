var autobahn = require('autobahn');

var connection = new autobahn.Connection({url: 'ws://5.141.98.216:50100/ws', realm: 'smart'});

connection.onopen = function (session) {

    session.call('smart.subs.get_devices', []).then(
        function (devices) {
            console.log(devices)
        }
    ).catch(
        function (err) {
            console.log(err)
        }
    );

    let subfunc = function (args) {
        console.log(args);
    };
    session.subscribe('smart.subs.function', subfunc);

    let setParam = function () {
        session.call('smart.subs.set_param_device', [55, 5, 1]).then(
            function () {
                console.log('FUNCTION COMPLETED')
            }
        ).catch(
            function (err) {
                console.log(err)
            }
        )
    };
    setTimeout(setParam, 1000)
};

connection.open();